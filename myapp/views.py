from django.shortcuts import render
from django.http.response import HttpResponse

y = 0
t = 0

####Viewer本文
from . import forms
from . import stock_predict
from . import stock_predict_nikkei
from . import brand_list
from . import read_nikkei
import requests


def index_template(request):
    d = {
        'is_visible': False,
        'empty_str': '',
        'unko': 'unko',
    }
    return render(request, 'index.html', d)

def hello_if(request):
    d = {
        'is_visible': False,
        'empty_str': '',
        'unko': 'unko',
    }
    return render(request, 'if.html', d)


def index_nikkei(request):
    brand_name = '5411'
    epoch = 3
    s_date = '2018-01-01'
    e_date = '2018-07-01'
    batch = 20
    ratio = 0.8
    
    #LSTMに渡す引数
    input = (
        brand_name,
        epoch,
        s_date,
        e_date,
        batch,
        ratio
    )   
    
    form = forms.HelloForm(request.GET or None)
    if form.is_valid():
        message = 'Success Data Communication'
        brand_name = form.cleaned_data['your_name']
        epoch = int(form.cleaned_data['epoch_number'])
        s_date = form.cleaned_data['s_date']
        e_date = form.cleaned_data['e_date']
        batch = int(form.cleaned_data['batch'])
        
        #LSTMに渡す引数
        input = (
            brand_name,
            epoch,
            s_date,
            e_date,
            batch,
            ratio,
        )   
    else:
        message = 'Failed Data Communication'
    
    result = stock_predict_nikkei.LSTM(input)
    
    y = result[0]
    t = result[1]
    
    #htmlに渡すもの
    d = {
        'brand_list':brand_list.brand_list,
        'form': form,
        'message': message,
        'y':y,
        't':t,
        'num' : range(12),
    }
    
    return render(request, 'nikkei.html', d)